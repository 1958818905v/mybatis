package com.gao.dao;

import com.gao.pojo.User;

import java.util.List;

public interface UserMapper {
    /**
     * 查询全部用户
     */
    List<User> getUserList ();

    /**
     * 根据Id查询用户
     */
    User getUserById (int ID);
}
